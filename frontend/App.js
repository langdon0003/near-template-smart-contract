import "regenerator-runtime/runtime";
import React from "react";

import "./assets/global.css";

import { EducationalText, SignInPrompt, SignOutButton } from "./ui-components";

export default function App({ isSignedIn, contractId, wallet }) {
  const [valueFromBlockchain, setValueFromBlockchain] = React.useState();

  const [uiPleaseWait, setUiPleaseWait] = React.useState(true);

  // Get blockchian state once on component load
  React.useEffect(() => {
    getGreeting()
      .then(setValueFromBlockchain)
      .catch(alert)
      .finally(() => {
        setUiPleaseWait(false);
      });
  }, []);

  // Get blockchian state once on component load
  function contractViewCall() {
    // use the wallet to query the contract's greeting
    return wallet.viewMethod({ method: "get_subscriber_users", contractId });
  }
  function contractViewCallThread() {
    // use the wallet to query the contract's greeting
    return wallet.viewMethod({
      method: "get_thread_metadata_by_thread_id",
      args: { thread_id: "gmfam.testnet_thread_title_01" },
      contractId,
    });
  }
  React.useEffect(() => {
    contractViewCallThread()
      .then((res) => console.log("contractViewCallThread res", res))
      .catch(alert)
      .finally(() => {
        setUiPleaseWait(false);
      });
  }, []);

  /// If user not signed-in with wallet - show prompt
  if (!isSignedIn) {
    // Sign-in flow will reload the page later
    return (
      <SignInPrompt
        greeting={valueFromBlockchain}
        onClick={() => wallet.signIn()}
      />
    );
  }

  function changeGreeting(e) {
    e.preventDefault();
    setUiPleaseWait(true);
    const { greetingInput } = e.target.elements;

    // // use the wallet to send the greeting to the contract
    wallet
      .callMethod({
        method: "vote_thread",
        args: {
          thread_id: "gmfam.testnet_thread_title_22",
          choice_number: 1,
          point: 100,
        },
        contractId,
      })
      .then((res) => console.log("vote_thread res", res))
      .catch((error) => console.log("error kind", error?.kind?.ExecutionError));
    // use the wallet to send the greeting to the contract
    // wallet
    //   .callMethod({
    //     method: "create_user",
    //     args: {
    //       nickname: "longn",
    //     },
    //     contractId,
    //   })
    //   .then((res) => console.log("vote_thread res", res))
    //   .catch((error) => console.log("error kind", error?.kind?.ExecutionError));
    // wallet
    //   .callMethod({
    //     method: "active_user_role",
    //     args: {
    //       user_id: "longnt.testnet",
    //     },
    //     contractId,
    //   })
    //   .then((res) => console.log("vote_thread res", res))
    //   .catch((error) => console.log("error kind", error?.kind?.ExecutionError));
  }

  function getGreeting() {
    // use the wallet to query the contract's greeting
    return wallet.viewMethod({ method: "get_greeting", contractId });
  }

  return (
    <>
      <SignOutButton
        accountId={wallet.accountId}
        onClick={() => wallet.signOut()}
      />
      <main className={uiPleaseWait ? "please-wait" : ""}>
        <h1>
          The contract says:{" "}
          <span className="greeting">{valueFromBlockchain}</span>
        </h1>
        <form onSubmit={changeGreeting} className="change">
          <label>Change greeting:</label>
          <div>
            <input
              autoComplete="off"
              defaultValue={valueFromBlockchain}
              id="greetingInput"
            />
            <button>
              <span>Save</span>
              <div className="loader"></div>
            </button>
          </div>
        </form>
        <EducationalText />
      </main>
    </>
  );
}
